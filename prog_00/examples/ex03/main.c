#include <avr/io.h>

void ft_delay_ms(unsigned int n){ // don't work with -Os
	unsigned int x;

	while(n--){
		x=2300;
		while(x--);
	}
}

int	main(void){
	DDRB |= (1 << PORTB3); // OUTPUT
	for(;;) {
		PORTB ^= (1 << PORTB3);
		ft_delay_ms(500);
	}
}
