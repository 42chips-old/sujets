- Preparation:
    - Preparer kits TP (voir table)
    - Flasher le bootloader [MiniCore](https://github.com/MCUdude/MiniCore) sur les atmega328p ( Ext 16Mhz, BOD 2.7 )
    - Flasher le code ex06 (mais avec les LEDs allumé par default)
- debut TP:
  - Distribuer les kits TP
  - Expliquer comment marche une breadboard 
- Verification breadboard:
  - pas de cc entre le 5V et GND
  - les 4 LEDs s'allume quand 5V
  - quand on appui sur le boutton ça compte en binaire
  - reset fonction
  - si tout est bon le groupe peut continuer le TP


# Kit TP:
![](prog_00/subject/kits.jpg)

# Example de breadboard
![](prog_00/subject/breadboard_photo.jpg)

# Materiel:
|Reference  |Quantity|Value         |lcsc#  |link                                                   |
|-----------|--------|--------------|-------|-------------------------------------------------------|
|C1         |1       |10uF          |C43351 |                                                       |
|C2 C3      |2       |100nF         |C353909|                                                       |
|C4 C5      |2       |22pF          |C254096|                                                       |
|D1 D2 D3 D4|4       |LED           |C440523|                                                       |
|R1 R6      |2       |10k           |C119894|                                                       |
|R2 R3 R4 R5|4       |130R          |C167536|                                                       |
|SW1 SW2    |2       |Button        |C393938|                                                       |
|TP1        |1       |breadboard    |C93522 |                                                       |
|U1         |1       |USB_UART      |   x   |[Link](https://fr.aliexpress.com/item/32750167241.html)|
|U2         |1       |atmega328     |C33901 |                                                       |
|Y1         |1       |16MHz         |C16212 |                                                       |

