## TP
- Programation:
  - prog_0: [sujet](https://gitlab.com/42chips/pdf/-/raw/master/prog_00.fr.pdf), [example](prog_00/examples)
  - prog_1: [sujet](https://gitlab.com/42chips/pdf/-/raw/master/prog_01.fr.pdf), [example](prog_01/examples)
  - prog_2: [sujet](https://gitlab.com/42chips/pdf/-/raw/master/prog_02.fr.pdf), [example](prog_02/examples)
  - prog_3: [sujet](https://gitlab.com/42chips/pdf/-/raw/master/prog_03.fr.pdf), [example](prog_03/examples)
  - ressource: [prepa](https://gitlab.com/42chips/sujets/-/blob/master/prog_00/subject/README.md), [shema](https://gitlab.com/42chips/tp_pdip), [video](https://www.youtube.com/watch?v=bzUYar4o2YM&list=PL45wZPWoD9vTDl3GQ8-td_jRp8xq74M4U)
- Tech: 
  - tech_0: [sujet](https://gitlab.com/42chips/pdf/-/raw/master/tech_00.fr.pdf)
  - tech_1: [sujet](https://gitlab.com/42chips/pdf/-/raw/master/tech_01.fr.pdf)
  - ressouce: [pcb](https://gitlab.com/42chips/tp_smd)
- CAO:
  - cao_0: [sujet](https://gitlab.com/42chips/pdf/-/raw/master/cao_00.fr.pdf), [shema](cao_00/subject/tp_smd.pdf), [BOM](cao_00/subject/BOM.csv)
  - cao_1: [sujet](https://gitlab.com/42chips/pdf/-/raw/master/cao_01.fr.pdf)
  - cao_2: [sujet](https://gitlab.com/42chips/pdf/-/raw/master/cao_02.fr.pdf)
  - ressource: [pcb](https://gitlab.com/42chips/tp_smd)

projet minimum:
  - 1 MCU: famille MegaAVRs ( non megaAVR 0-series )
  - au moins 1 périphérique en plus de l'UART (debug + flash) (i2C, SPI, ADC, ...)
  - pas de lib


example: project smart pot iot
  - mcu: ATmega328PB ( car x2 UART )
  - 1 UART: flash + debug
  - 1 UART: at-cmd vers esp8266 (wifi)
  - ADC: LDR sensor
  - I2C: temp + humidity sensor
  - regu lineaire 3.3V


example: radio tx
  - mcu: ATmega328P
  - 1 UART: flash + debug
  - 1 SPI: nRF24l01+ (2.4Ghz radio)
  - ADC: x4 stick input
  - regu lineaire 5V


example: midi controler
  - mcu: ATmega2560
  - 1 UART: flash + debug
  - 1 UART: midi Out
  - 1 UART: midi In
  - ADC: x16 potentiometer
  - Para: LCD 16x04 